package com.example.calculadorakotlin

class Calculadora {
    var num1: Int = 0
    var num2: Int = 0
    constructor(num1: Int, num2: Int){
        this.num1 = num1
        this.num2 = num2
    }
    fun suma():Int{
        var var1 = num1
        var var2 = num2
        var total = 0
        total = var1 + var2
        return total
    }
    fun resta():Int{
        var var1 = num1
        var var2 = num2
        var total = 0
        total = var1 - var2
        return total
    }
    fun multiplicacion():Int{
        var var1 = num1
        var var2 = num2
        var total = 0
        total = var1 * var2
        return total
    }
    fun division():Int{
        var var1 = num1
        var var2 = num2
        var total = 0
        if (var2 != 0) {
            total = var1 / var2
        }
        return total
    }
}