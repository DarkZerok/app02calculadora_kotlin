package com.example.calculadorakotlin

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var btnsuma:Button
    private lateinit var btnresta:Button
    private lateinit var btnmult:Button
    private lateinit var btndiv:Button
    private lateinit var btnlimp:Button
    private lateinit var btnsalir:Button
    private lateinit var text_respuesta:TextView
    private lateinit var editUno:EditText
    private lateinit var editDos:EditText
    private lateinit var calculadora:Calculadora

    private fun btnSuma(){
        calculadora = Calculadora(
            editUno.text.toString().toInt(),
            editDos.text.toString().toInt()
        )
        var total = calculadora.suma();
        text_respuesta.text = "" +total;
    }
    private fun btnResta(){
        calculadora = Calculadora(
            editUno.text.toString().toInt(),
            editDos.text.toString().toInt()
        )
        var total = calculadora.resta();
        text_respuesta.text = "" +total;
    }
    private fun btnMult(){
        calculadora = Calculadora(
            editUno.text.toString().toInt(),
            editDos.text.toString().toInt()
        )
        var total = calculadora.multiplicacion();
        text_respuesta.text = "" +total;
    }
    private fun btnDiv(){
        calculadora = Calculadora(
            editUno.text.toString().toInt(),
            editDos.text.toString().toInt()
        )
        var total = calculadora.division();
        text_respuesta.text = "" +total;
    }
    private fun btnLimp(){
        text_respuesta.setText("")
        editUno.setText("")
        editDos.setText("")
    }
    private fun btnSalir(){
        var confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("¿CERRAR APP?")
        confirmar.setMessage("Se descartará toda la información")
        confirmar.setPositiveButton("Confirmar"){dialogInterface, which-> finish()}
        confirmar.setNegativeButton("Cancelar"){dialogInterface, which->}
        confirmar.show()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnsuma = findViewById(R.id.button_suma)
        btnresta = findViewById(R.id.button_resta)
        btnmult = findViewById(R.id.button_multi)
        btndiv = findViewById(R.id.button_divi)
        btnlimp = findViewById(R.id.button_limpiar)
        btnsalir = findViewById(R.id.button_salir)
        text_respuesta = findViewById(R.id.resultado)
        editUno = findViewById(R.id.num1)
        editDos = findViewById(R.id.num2)
        btnlimp.setOnClickListener(View.OnClickListener{ btnLimp()})
        btnsalir.setOnClickListener(View.OnClickListener{ btnSalir()})
        btnsuma.setOnClickListener(View.OnClickListener{ btnSuma()})
        btnresta.setOnClickListener(View.OnClickListener{ btnResta()})
        btnmult.setOnClickListener(View.OnClickListener{ btnMult()})
        btndiv.setOnClickListener(View.OnClickListener{ btnDiv()})
    }
}